const partners = [
    'Cerego',
    'CogBooks',
    'Connect for Education',
    'Courseweaver',
    'eMath',
    'Expert TA',
    'Lrnr',
    'Lumen',
    'Memory Science',
    'Odigia',
    'panOpen',
    'Sapling Learning',
    'SimBio',
    'Top Hat',
    'WebAssign',
    'Wiley',
    'XYZ Homework'
];

export default partners;
