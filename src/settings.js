const settings = {
    titleSuffix: ' - OpenStax',
    analyticsID: 'UA-73668038-1',
    tagManagerID: 'GTM-W6N7PB',
    webmaster: 'webmaster@openstax.org',
    apiOrigin: 'https://oscms-dev.openstax.org',
    accountHref: 'http://accounts.openstax.org'
};

export default settings;
